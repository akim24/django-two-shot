from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptForm(ModelForm):
    def __init__(self, user_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if user_id is not None:
            self.fields["category"].queryset = ExpenseCategory.objects.filter(owner=user_id)
            self.fields["account"].queryset = Account.objects.filter(owner=user_id)

    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
